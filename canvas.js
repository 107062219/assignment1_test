var canvas = document.getElementById('pic');
var ctx = canvas.getContext('2d');

ctx.strokeStyle = '#000';
ctx.lineJoin = 'round';
ctx.lineCap = 'round';
ctx.lineWidth = 8;

let isDrawing = false;
var mode = "pencil";

let lastX = 0;
let lastY = 0;
let draw_x = 0;
let draw_y = 0;

let word;
let word_size;
let font;

var undo_list=[];
var redo_list=[];

let isFilled = 1;

canvas.addEventListener('mouseup', ()=> isDrawing = false);
canvas.addEventListener('mouseout', ()=> isDrawing = false);
canvas.addEventListener('mousedown', (e) => {

    word = document.getElementById('words').value;
    //word_size = document.getElementsById('word_size').value;
    //font = document.getElementsById('font').value;
    ctx.font = word_size+"px "+font; 
    ctx.fillStyle = document.getElementById("favcolor").value;

    var tmp = ctx.getImageData(0,0,canvas.width,canvas.height);
    undo_list.push(tmp);
    redo_list = [];
    ctx.save();

    isDrawing = true;
    [lastX, lastY] = [e.offsetX, e.offsetY];
    [draw_x, draw_y] = [e.offsetX, e.offsetY];

});

canvas.addEventListener('mousemove', draw);

function draw(e){
    if(!isDrawing) return;

    ctx.beginPath();
    //pencil
    if(mode == "pencil"){
        ctx.globalCompositeOperation="source-over";
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
    }
    //eraser
    else if(mode == "eraser"){
        ctx.globalCompositeOperation="destination-out";
        ctx.arc(lastX,lastY,ctx.lineWidth,0,Math.PI*2,false);//size
        ctx.fill();
    }
    //circle
    else if(mode == "circle"){
        //prevent multiple circle
        if(undo_list.length>0){
            var tmp = undo_list.pop();
            ctx.putImageData(tmp,0,0);
            undo_list.push(tmp);
        }
        
        ctx.globalCompositeOperation="source-over";
        ctx.arc((draw_x+lastX)/2,(draw_y+lastY)/2,dis()/2,0,Math.PI*2,false);
        if(isFilled){
            ctx.fill();
        }
        else{
            ctx.stroke();
        }
    }
    //rectangle
    else if(mode == "rectangle"){
        if(undo_list.length>0){
            var tmp = undo_list.pop();
            ctx.putImageData(tmp,0,0);
            undo_list.push(tmp);
        }

        ctx.globalCompositeOperation="source-over";
        ctx.rect(draw_x,draw_y,lastX-draw_x,lastY-draw_y);
        if(isFilled){
            ctx.fill();
        }
        else{
            ctx.stroke();
        }
    }
    //triangle
    else if(mode == "triangle"){
        if(undo_list.length>0){
            var tmp = undo_list.pop();
            ctx.putImageData(tmp,0,0);
            undo_list.push(tmp);
        }

        ctx.globalCompositeOperation="source-over";
        ctx.moveTo(draw_x,draw_y);
        ctx.lineTo(lastX,lastY);
        ctx.lineTo(2*draw_x-lastX,lastY);
        ctx.closePath();
        if(isFilled){
            ctx.fill();
        }
        else{
            ctx.stroke();
        }
    }
    else if(mode == "word"){
        var tmp = undo_list.pop();
        ctx.putImageData(tmp,0,0);
        undo_list.push(tmp);
        ctx.fillText(word, e.offsetX, e.offsetY);
    }
    
    [lastX, lastY] = [e.offsetX, e.offsetY];
    
}
function undo(){
    if(undo_list.length>0){
        redo_list.push(ctx.getImageData(0,0,canvas.width,canvas.height));
        var tmp = undo_list.pop();
        ctx.putImageData(tmp,0,0);
    }
}
function redo(){
    if(redo_list.length>0){
        undo_list.push(ctx.getImageData(0,0,canvas.width,canvas.height));
        var tmp = redo_list.pop();
        ctx.putImageData(tmp,0,0);
    }
}
function reset(){
    var tmp = ctx.getImageData(0,0,canvas.width,canvas.height);
    undo_list.push(tmp);
    ctx.clearRect(0,0,canvas.width,canvas.height);
}

//write
/*function write(e){
    if(!isDrawing)return;
    var tmp = undo_list.pop();
    ctx.putImageData(tmp,0,0);
    undo_list.push(tmp);
    ctx.fillText(word, e.offsetX, e.offsetY);
}*/
function changeWord_size(){
    word_size = document.getElementById('word_size').value;
}
function changeFont(val){
    font = val;
}
function isfilled(){
    isFilled = !isFilled;
}

let fileInput = document.getElementById('upload');
fileInput.addEventListener('change', function(e){
    if(e.target.files){
        var img = new Image();
        let file = e.target.files[0];
        var src = URL.createObjectURL(file);
        img.src = src;

        img.onload = function(e){
            var tmp = ctx.getImageData(0,0,canvas.width,canvas.height);
            undo_list.push(tmp);
            ctx.clearRect(0,0,ctx.width,ctx.height);
            ctx.drawImage(img,0,0);
        }
    }
})

/*function upload(){
    var img = new Image();
    var file = this.files[0];
    var src = URL.createObjectURL(file);
    img.onload = function(){
        ctx.width = this.width;
        ctx.height = this.height;
        ctx.drawImage(this, 0, 0, ctx.width, ctx.height);
        URL.revokeObjectURL(src);
    } 
    img.src = src;
}
var now = new Image();
now.src = ctx.toDataURL();
ctx.clearRect(0, 0, canvas.width. canvas.height);
ctx.drawImage(now, 0, 0);*/

//size
function size_change(){
    ctx.lineWidth = document.getElementById("size").value;
}
//color
function color_change(){
    ctx.strokeStyle = document.getElementById("favcolor").value;
}

//distance
function dis(){
    var dx = Math.abs(lastX-draw_x);
    var dy = Math.abs(lastY-draw_y);
    var dist = Math.sqrt(Math.pow(dx,2)+Math.pow(dy,2));
    return dist;
}

//mode change
function mode_pencil(){
    mode = "pencil";
    document.getElementById('pic').className = "pen";
}
function mode_eraser(){
    mode = "eraser";
    document.getElementById('pic').className = "eraser";
}
function mode_circle(){
    mode = "circle";
    document.getElementById('pic').className = "circle";
}
function mode_rectangle(){
    mode = "rectangle";
    document.getElementById('pic').className = "rectangle";
}
function mode_triangle(){
    mode = "triangle";
    document.getElementById('pic').className = "triangle";
}
function mode_word(){
    mode = "word";
}
/*function mode_reset(){
    mode = "reset";
}*/

/*star*/
var stars = document.getElementById('stars')

// js随机生成流星
for (var j = 0; j < 20; j++) {
    var newStar = document.createElement("div")
    newStar.className = "star"
    newStar.style.top = randomDistance(500, -100) + 'px'
    newStar.style.left = randomDistance(1980, 300) + 'px'
    stars.appendChild(newStar)
}

// 封装随机数方法
function randomDistance(max, min) {
    var distance = Math.floor(Math.random() * (max - min + 1) + min)
    return distance
}

var star = document.getElementsByClassName('star')

// 给流星添加动画延时
for (var i = 0, len = star.length; i < len; i++) {　　
    star[i].style.animationDelay = i % 6 == 0 ? '0s' : i * 0.8 + 's'
}
